Usage: SplitKodakDisc.sh X1 Y1 X6 Y6 X11 Y11 ImageFile [DestDir]  
Where Xn Yn are the coordinates of the point where the two images near '^N' do touch each others.  
If DestDir is not given, the images are saved in the current directory

See https://www.gdargaud.net/Photo/KodakDiskScan.html for examples